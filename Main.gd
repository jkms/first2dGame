extends Node

@export var mob_scene: PackedScene
var score

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func game_over():
	$DeathSound.play()
	$ScoreTimer.stop()
	$MobTimer.stop()
	$HUD.show_game_over()
	$Music.stop()

func new_game():
	#print("new game")
	score = 0
	$Player.start($StartPosition.position)
	$StartTimer.start()
	$HUD.update_score(score)
	$HUD.show_message("Get Ready")
	get_tree().call_group("mobs", "queue_free")
	$Music.play() 

func _on_score_timer_timeout():
	score += 1
	$HUD.update_score(score)

func _on_start_timer_timeout():
	#print ("start timed out")
	$MobTimer.start()
	$ScoreTimer.start()

	
func _on_mob_timer_timeout():
	# Create a new instance of the Mob scene.
	var mob = mob_scene.instantiate()

	# Choose a random location on Path2D.
	var mob_spawn_location = $MobPath/MobSpawnLocation
	mob_spawn_location.progress_ratio = randf()

	# Set the mob's direction perpendicular to the path direction.
	var direction = mob_spawn_location.rotation + PI / 2

	# Set the mob's position to a random location.
	mob.position = mob_spawn_location.position

	# Add some randomness to the direction.
	direction += randf_range(-PI / 4, PI / 4)
	mob.rotation = direction
	
	# Vary the size and speed of the mob
	var mob_scale = randf_range(0, 100)

	# Set the velocity for the mob.
	var velocity = Vector2(mob_scale + 100, 0.0)
	mob.linear_velocity = velocity.rotated(direction)
	
	# Set the size of the mob
	mob.get_node("AnimatedSprite2D").scale = Vector2 (((100-mob_scale)/100) + 0.5,((100-mob_scale)/100) + 0.5)
	mob.get_node("CollisionShape2D").scale = Vector2 (((100-mob_scale)/100) + 0.5,((100-mob_scale)/100) + 0.5)
	mob.get_node("VisibleOnScreenNotifier2D").scale = Vector2 (((100-mob_scale)/100) + 0.5,((100-mob_scale)/100) + 0.5)

	# Spawn the mob by adding it to the Main scene.
	add_child(mob)



